using FantomesCICD;
using Xunit;

namespace FantomesCICD.test
{
    public class RomanConverterTest
    {
        [Fact]
        public void Test1()
        {
            var romanConverter = new RomanConverter();
            var romanNumber = romanConverter.ConvertToRoman(1);

            Assert.Equal("I", romanNumber);

        }
    }
}